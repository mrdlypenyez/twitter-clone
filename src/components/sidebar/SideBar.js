import React from "react";
import SubMenu from "./SubMenu"

const SideBar = () => {

    return (
        <div class="sidebar">
            <i class="fab fa-twitter"></i>
            <div class="sidebarOption active">
                <span class="material-icons"> home </span>
                <h2>Home</h2>
            </div>
            <SubMenu title="Explore" icon="search"/>
            <SubMenu title="Notifications" icon="notifications_none"/>
            <SubMenu title="Messages" icon="mail_outline"/>
            <SubMenu title="Bookmarks" icon="bookmark_border"/>
            <SubMenu title="Lists" icon="list_alt"/>
            <SubMenu title="Profile" icon="perm_identity"/>
            <SubMenu title="More" icon="more_horiz"/>
            
            <button class="sidebar__tweet">Tweet</button>
        </div>
    )
}
export default SideBar;