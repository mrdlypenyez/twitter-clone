
import React from "react";
const SubMenu = ({ title, icon}) => {

    
    return (
        <>
        <div class="sidebarOption">
                <span class="material-icons">{icon} </span>
                <h2>{title}</h2>
            </div>
        </>
    )
}
export default SubMenu;