

const SearchWidget=()=>{
    return(
        <div class="widgets__input">
        <span class="material-icons widgets__searchIcon"> search </span>
        <input type="text" placeholder="Search Twitter" />
      </div>
    )
}

export default SearchWidget;