import UserCart from "../user/UserCart";


const PostItem = ({imageUrl,uName,userName,description,descriptionImage}) => {

    return (

        <>
           <UserCart imageUrl={imageUrl} name={uName} userName={userName} />
            <div class="post__body">
                <div class="post__header">
                    <div class="post__headerDescription">
                        <p>{description}</p>
                    </div>
                </div>
                <img
                    src={descriptionImage}
                    alt=""
                />
                <div class="post__footer">
                    <span class="material-icons"> repeat </span>
                    <span class="material-icons"> favorite_border </span>
                    <span class="material-icons"> publish </span>
                </div>
            </div>

        </>

    )
}

export default PostItem;