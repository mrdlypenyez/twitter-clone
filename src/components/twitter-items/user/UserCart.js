

const UserCart = ({imageUrl,name,userName }) => {

    return (
        <>
            <div class="post__avatar">
                <img
                    src={imageUrl}
                    alt=""
                />
            </div>

            <div class="post__body">
                <div class="post__header">
                    <div class="post__headerText">
                        <h3>
                            {name}
                            <span class="post__headerSpecial"
                            ><span class="material-icons post__badge"> verified </span>{userName}</span
                            >
                        </h3>
                    </div>
                </div>
            </div>
        </>
    )

}

export default UserCart;