import React from 'react';
import './navbar.css';
const formCenter = {
    margin: "0 auto",
    float: "none",
    padding: "15px"
}
const Navbar = () => {

    return (
        <div class="row">
            <div class="col-lg-3 col-centered" style={formCenter}>

                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fas fa-long-arrow-alt-left"></i>
                        </button>
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </span>

                    <input type="text" class="search-text" />

                </div>
            </div>
        </div>
    )

}
export default Navbar;