
import Navbar from './components/navbar/Navbar';
import './App.css';
import SideBar from './components/sidebar/SideBar'
import FeedContent from './components/twitter-items/FeedContent'
import React from 'react';
import Widget from './components/twitter-items/widget/Widget';


function App() {
  return (
    <React.Fragment>
      <SideBar />
      <FeedContent />
      <Widget/>
    </React.Fragment>


  );
}

export default App;
